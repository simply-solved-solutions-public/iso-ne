FROM python:3.6

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV dev
ENV DOCKER_CONTAINER 1

COPY . /code/
WORKDIR /code/
RUN pip install -r /code/requirements.txt


CMD ["sh", "-c", "/usr/local/bin/python /code/manage.py migrate --noinput && /usr/local/bin/python /code/manage.py runserver 0.0.0.0:8000"]

EXPOSE 8000

