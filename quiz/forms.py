from django import forms
from .models import UserModel

class View1Form(forms.ModelForm):
    class Meta:
        model = UserModel
        fields = ["field1"]
