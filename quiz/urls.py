from django.urls import path
from . import views

urlpatterns = [
    path('', views.CreateView1.as_view(), name='create-view1'),
    path('submissions', views.ListView1.as_view(), name='list-view1'),
    path('search/', views.SearchView.as_view(), name='search-view'),
#    path('search/<query>/', views.SearchView.as_view(), name='search-view'),
    path('submitted', views.submitted, name='submitted'),
]
