from django.shortcuts import render
from django.views.generic.edit import CreateView
from django.views.generic import ListView, FormView
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.db.models import Q
from .models import UserModel, ComputedModel



class CreateView1(CreateView):
    model = UserModel
    fields = ['field1']

    success_url = reverse_lazy('submitted') 

class ListView1(ListView):
    model = UserModel

class SearchView(ListView):
    title = 'Search Results'
    template_name = 'quiz/search_results.html'
    context_object_name = 'results'

    def get_queryset(self, **kwargs):
        query = self.request.GET.get('query', None)
        q = Q(usermodel__field1__icontains=query) | Q(function_output__icontains=query)
        queryset = ComputedModel.objects.select_related("usermodel").filter(q)
        return queryset

def submitted(request):
    return render(request,"quiz/submitted.html", {})

