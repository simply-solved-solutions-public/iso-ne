DevOps Sample Project
=====================

Create a vanilla Django project with startproject. Create two views in your web app:

### View 1

View 1 is a web form that accepts at least one user input. Upon form submission, route the user input(s) to 100 different lambdas that each transform the input in some way, writing all transformed values back to a persistent datastore.

### View 2

View 2 is a search input that queries all data in the persistent datastore and displays the results in a datagrid.

Deployment
----------

Deploy this project in a way that scales to 1 million concurrent users:

-   Provide configuration for the AWS services you would use.
-   If any scripts are needed, provide them.
-   Document how your deployment process works in a way management can read and understand.
-   Document the detailed steps one must take for deploying the site from scratch.
-   If subsequent deployments of the site are different from the “from scratch” way of deployment, write up the steps and reasoning.
-   Put all of this in a GitHub or Gitlab repo.

Documentation is particularly important here. Imagine you are going on a 1-month vacation. The docs must contain exactly what any member of the DevOps team needs to know to deploy the project without contacting you.

Rules
-----

= This isn't a new problem. Reuse of existing work is fine, even encouraged, but you have to cite what you reuse and document your reasons for doing so.

-   We use Python and Docker a lot. Keep that in mind. 
-   Do not include these AWS services:
    -   Elastic Beanstalk
-   Bonus points for using these AWS services:
    -   CloudFormation
-   Bonus points for considering these in your approach and documentation:
    -   12-Factor App pattern
    -   SAFe (Scaled Agile Framework) Release on Demand principle

Guidelines
----------

We're looking for someone who can work independently and is curious and self-motivated. One major goal of this project is to see how you fill in ambiguities creatively. There is no such thing as a perfect project here, just interpretations of the instructions above, so be creative in your approach.


Project Approach and Deployment Method
=======================

### Application Overview


In creating this Django web app, a major consideration was scalability. With this in mind, the application's user-facing web service was decoupled from backend processing.  The user-facing portion of the application consisted of two views, namely the user input being submitted (View 1), and the user search page that lists the results of the lambda function transformations (View 2). Once user input is submitted and saved to the datastore, 100 lambda functions are asynchronously queued up for processing by separate celery worker processes with their results stored in the datastore.  This decoupled approach allows the front-end and back-end services to be independently scaled horizontally by adding additional docker instances as needed.

### AWS Services

The deployment of this project consisted of the following AWS services:
- Elastic Container Service (ECS):  Provides a managed docker cluster for deploying the dockerized Django web app.  The web app is deployed as two services using the same image. There is the front-end service serving end-user web requests, and the backend-end service used to process the lambda functions brought in from the redis queue. 
- PostgreSQL RDS:  Managed datastore for the web app used to store user submissions as well as the output from lambda function transformation.
- Elastic Container Registry (ECR):  Stores and versions docker images built during build process and is used by ECS when service tasks are run.  
- Elasticache Redis:  Managed redis cluster to serve as the queue which receives jobs from the front-end webserver and routes them to the backend worker processes so the lambda functions can be asynchronously processed.  
- Elastic Load Balancing:  Allows the front-end webserver user requests to be routed to the various web server instances created by the auto-scaling policies of the Application Auto Scaling service. This allows our web app to respond to user demand as needed.
- CloudFormation:  Allows for deploying our application using infrastructure-as-code which can be stored in version control.  Allows for application deployment to be easily and precisely replicated, and additionally, allows for infrastructure updates to be tested in non-production environments before being applied to production.
- Cloudwatch:  Agregates all logs from various sources which can be acted upon by configuring events of interest.  Also provides the much needed information to debug issues if and when they arise.

### Application Development

This django application is built as a docker image and the complete environment including web, celery, redis, and database services are brought up using docker-compose.   

#### Directory Structure

*  iso-ne/ # project directory containing Django project settings
*  quiz/      # project app directory containing models, functions, and views for View 1, View2, and Lambda functions
*  deploy/    # contains Ansible playbook and CloudFormation template for deploying and updating this project on AWS

#### To Develop Locally:
1. Clone this repository to your working environment. 
2. Create and activate python virtual environment
*  `virtualenv -p python36 env3`
*  `source env3/bin/activate`
3. Install project requirements
*  `pip install -r requirements.txt`
*  `pip install -r deploy/requirements.txt`
4. Make any desired updates to the application.
5. Create database migrations and build application using:
*  `python manage.py makemigrations`
*  `docker-compose build`
6. Launch application and dependent services
*  `docker-compose up`
7. Application is locally availble at [http://localhost:8000/](http://localhost:8000/)

#### Project Deployment to AWS:
This project can be both initially deployed as well as subsequently updated using the Ansible Playbook located under the deploy/ directory.  In order to deploy this project to AWS, an AWS account is required along with AWS credentials stored in ~/.aws/credentials.  Your SSH key needs to be added to your AWS account and is used by the CloudFormation template to allow SSH access to the EC2 instances created by ECS.  The ssh Keyname needs to be set as an environment variable called AWS_EC2_KEYNAME.  

Assuming the python virtual environment was created and requirements installed as described above, the project can be built and deployed as follows:

`ansible-playbook -i deploy/inventory deploy/site.yml`

If this is a 'from scratch' deployment, this may take a little while (20-30 minutes).  The playbook will start by obtaining ECR credentials and use them to connect your local docker daemon to ECR.  Then it will ensure a docker registry is created (iso-ne/iso-ne in this case) and will proceed by building the web app's docker image and pushing it to ECR.  Finally, the CloudFormation stack is deployed which will create the entire environment using the AWS services described above.  By the time the playbook completes executing, a complete and scalable environment will have been built.  

As updates are made to the application and deployment codebase, the playbook can be re-executed (using same command as above) so that new docker images can be deployed and/or CloudFormation updates applied.

#### Areas of Improvement
- Add an actual webserver such as Nginx or Apache in front of the Django application using WSGI.
- Create a GitLab CI Pipeline in this repository with branches for integration, staging, and production environments.  Further build out Ansible inventory to abstract envrionment specific variables.  Once in place, developers should be able to push code to repository and have it automatically built and deployed to integration environment.  If tests pass, changes can be merged to staging branch for deployment into staging environment for user acceptance testing.  If changes are approved, changes can finally be merged to production branch for production release.
- Add unit and functional tests
- Replace need for python virtual environment for making database migrations and using Ansible with docker workflow.   
- Update load balancer to redirect port 80 to 443 while using SSL certificate for branded domain.
